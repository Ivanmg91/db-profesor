use videoclub;
-- Lista películas junto a todos los actores que la interpretaron
SELECT
	P.Titol AS "Título de la película",
    A.Nom AS "ACTOR"
FROM PELICULA AS P
JOIN INTERPRETADA AS I
ON P.CodiPeli = I.CodiPeli
JOIN ACTOR AS A
ON I.CodiActor = A.CodiActor;