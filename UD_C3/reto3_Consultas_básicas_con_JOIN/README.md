# Reto 3: Consultas básicas con JOIN

Iván Montiano González.

En este reto trabajamos con la base de datos `Videoclub`, que nos vienen dadas en el fichero `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados. En todas las querys usamos `JOIN`, que nos va a permitr combinar registros entre varias tablas.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Ivanmg91/db-profesor/-/tree/main/UD_C1/reto2_Consultas_b%C3%A1sicas_2

## Query 1
Listar todas las películas del videoclub junto al nombre de su género. Para hacerlo, vamos a utilizar `SELECT` para seleccionar las columnas de las tablas que queremos, luego `FROM` para especificar la tabla que contiene los datos que vamos a usar y por ultimo `INNER JOIN` para combinar las filas que con el mismo valor en la columna de `CodiGenere`.

```sql
use videoclub;
SELECT PELICULA.Titol, GENERE.Descripcio
FROM PELICULA
INNER JOIN GENERE ON PELICULA.CodiGenere = GENERE.CodiGenere;
```

## Query 2
Listar todas las facturas de María. Utilizamos un `SELECT` para las columnas que necesitamos, `FROM` inidcando la tabla que vamos a usar. Luego, usamos `JOIN` para enlazar la columna `DNI` de la tabla factura con el de la tabla `CLIENT`.

```sql
use videoclub;
SELECT FACTURA.*, CLIENT.NOM
FROM FACTURA 
JOIN CLIENT 
ON FACTURA.DNI = CLIENT.DNI
WHERE NOM LIKE "Maria%";
```

## Query 3
Listar las películas junto a su actor principal. Utilizamos un `SELECT` para las columnas que necesitamos, `FROM` inidcando la tabla que vamos a usar. Luego, usamos `JOIN` para enlazar las columnas CodiActor de las tablas `PELICULA` Y `ACTOR`.

```sql
use videoclub;
SELECT
	P.Titol AS "Título de la película", 
	A.Nom AS "Actor principal"
FROM PELICULA AS P
JOIN ACTOR AS A
ON P.CodiActor = A.CodiActor;
```

## Query 4
-- Lista películas junto a todos los actores que la interpretaron. Utilizamos un `SELECT` para las columnas que necesitamos, `FROM` inidcando la tabla que vamos a usar. Luego, usamos dos `JOIN` para enlazar varias columnas y mostrar todas las peliculas aunque se repitan, con cada actor que la interpretó.

```sql
use videoclub;
SELECT
	P.Titol AS "Título de la película",
    A.Nom AS "ACTOR"
FROM PELICULA AS P
JOIN INTERPRETADA AS I
ON P.CodiPeli = I.CodiPeli
JOIN ACTOR AS A
ON I.CodiActor = A.CodiActor;
```