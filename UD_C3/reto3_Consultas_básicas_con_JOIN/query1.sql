use videoclub;
-- Lista todas las películas del videoclub junto al nombre de su género
SELECT PELICULA.Titol, GENERE.Descripcio
FROM PELICULA
INNER JOIN GENERE ON PELICULA.CodiGenere = GENERE.CodiGenere;
