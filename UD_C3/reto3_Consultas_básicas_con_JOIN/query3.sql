use videoclub;
SELECT
	P.Titol AS "Título de la película", 
	A.Nom AS "Actor principal"
FROM PELICULA AS P
JOIN ACTOR AS A
ON P.CodiActor = A.CodiActor;