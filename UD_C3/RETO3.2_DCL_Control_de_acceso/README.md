# Reto 3.2: Reto 3.2 DCL Control de acceso

Iván Montiano González.

En este reto trabajamos acerca de las opciones de control de acceso que ofrece MySQL.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Ivanmg91/db-profesor/-/tree/main/UD_C3/RETO3.2_DCL_Control_de_acceso?ref_type=heads

Lo primero que tenemos que hacer para poder trabajar con nuestra base de datos es conectarnos a ella. Para ello utilizaremos la terminal y escribiremos `mysql -u root -p -h 127.0.0.1 -P 33006` e introducimos nuestra contraseña. Ahora ya tenemos las funcionalidades de las querys en nuestra terminal.

Para registrar usuarios lo hariamos igual que en una query: `CREATE USER 'juan@localhost' IDENTIFIED BY 'contraseña';`, sin olvidar el ";" final. Si quisieramos borrarlo: `DROP USER 'john@localhost' IDENTIFIED BY 'contraseña';` y para modificarlo `ALTER USER 'juan@localhost' IDENTIFIED BY 'contraseña_nueva';`.

Además, podemos mostrar los permisos de un usuario con `SHOW GRANTS FOR 'juan@localhost';`

Normalmente, los usuarios deben autenticarse en el SGBD y lo pueden hacer de varias maneras. Las más comunes son la 2FA (autenticación de dos factores), la autenticación con contraseña y la autenticación basada en certificados.

Los usuarios pueden tener diferentes permisos segun su cargo, nivel... Por ejemplo, en una empresa los administradores tendrán permisos para añadir, modificar o borrar cosas de la base de datos, mientras que un trabajador de nivel más bajo tan solo podrá modificar los datos para revisarlos y un cliente igual puede acceder a los datos que necesite pero tan solo leerlos.
Un administrativo podrá gestionar usuarios y sus permisos.

Para que un admisitrativo pueda gestionar los usuarios y sus permisos necesita privilegios como: `CREATE USER`, `ALTER USER`. `DROP USER` y `GRANT OPTION`.

Algunos SGBD nos permiten tener usuarios dentro de un grupo o asignar roles a los diferentes usuarios. Esto nos puede ser muy útil para mantener a los usuarios con sus permisos bien organizados de forma segura. Además permite a los administradores trabajar con los sus datos y permisos de manera más eficiente.