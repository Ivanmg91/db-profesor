mysql -u root -p -h 127.0.0.1 -P 33006

-p para autenticarnos con contraseña

> SHOW COLUMNS FROM YOUR_TABLE
> SHOW DATABASES;
> SELECT * FROM mysql.user;
> SELECT host, user FROM mysql.user;
> SHOW GRANTS FRON 'cristian'@'%';
> CREATE USER 'estebanquito'@'192.168.%' IDENTIFIED BY '1234';
> GRANT SELECT ON CHINOOK.Playlist TO 'estebanquito'@'192.168.%';
> CREATE ROLE 'admin', 'developer';
> GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%'
> GRANT SELECT ON Chinook.Playlist TO 'admin'
> GRANT 'estebanquito' TO 'estebanquito'@'%'

> GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP
    -> ON codigofacilito.*
    -> TO 'nombre_usuario'@'localhost';
FLUSH PRIVILEGES;

> SET ROLE admin; si un usuario tiene varios roles con este comando puede cambiar de rol entre los que tiene. 

> SET DEFAULT ROLE admin TO "usuario de prueba"

> SELECT id FROM information_schema.processlist WHERE user = "ivan"

> kill ...

> REVOKE ...

show tables from performance_schema;

campo generado: añadir restricciones de integridad y acceder al valor de los campos a traves de una expresion

cross join
full join

SELECT user, host FROM mysql.user


EJERCICIO 14
con mariateresa (show grants, admin de chinook y profe)

SET ROLE "Chinook Admin",Profesor;
SHOW TABLES FROM Chinook;
DESCRIBE Chinook.Costumer;
DESCRIBE Facultad.Chinook;

SELECT * FROM Chinook.Customer JOIN Facultad.profesor ON Chinook.customer.email = Facultad.profesor.email;
SELECT * FROM Chinook.Customer JOIN Facultad.alumno ON Chinook.customer.email = Facultad.alumno.email;

Respuesta: ninguno, no hay ningun profesor en chinook, solo hay alumnos.

campo generado: añadir restricciones de integridad y acceder al valor de los campos a traves de una expresion

cross join
full join