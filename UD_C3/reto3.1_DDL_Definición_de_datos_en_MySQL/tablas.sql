CREATE TABLE Vuelos (
    id_vuelo INT PRIMARY KEY,
    origen VARCHAR(255),
    destino VARCHAR(255),
    fecha DATE,
    capacidad INT
);

CREATE TABLE Pasajeros (
    id_pasajero INT PRIMARY KEY,
    número_pasaporte VARCHAR(255),
    nombre_pasajero VARCHAR(255)
);

CREATE TABLE Vuelos_Pasajeros (
    id_vuelo INT,
    id_pasajero INT,
    n_asiento VARCHAR(255),
    PRIMARY KEY (id_vuelo, id_pasajero),
    FOREIGN KEY (id_vuelo) REFERENCES Vuelos(id_vuelo),
    FOREIGN KEY (id_pasajero) REFERENCES Pasajeros(id_pasajero)
);