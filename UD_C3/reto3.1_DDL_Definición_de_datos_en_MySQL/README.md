# Reto 3: Definición de datos en MySQL

Iván Montiano González.

En este reto documentaremos el proceso de definición de la estructura de datos para una aplicación de gestión de vuelos y la inserción de algunos de los registros en la base de datos creada. Además, incluiremos una reflexión sobre las restricciones definidas, las claves foráneas y demás.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Ivanmg91/db-profesor/-/tree/main/UD_C1/reto2_Consultas_b%C3%A1sicas_2

Para crear la aplicación, en este caso, necesitaremos 3 dentro de nuestra base de datos. Cada una de estas tablas se encargará de realizar una función:

La tabla vuelos almacenará el id del vuelo, el origen y el destino, la fecha del vuelo y su capacidad de pasajeros.

```sql
CREATE TABLE Vuelos (
    id_vuelo INT PRIMARY KEY,
    origen VARCHAR(255),
    destino VARCHAR(255),
    fecha DATE,
    capacidad INT
);
```

La tabla pasajeros almacenará el id de los pasajeros, su número de pasaporte y el nombre del pasajero.

```sql
CREATE TABLE Pasajeros (
    id_pasajero INT PRIMARY KEY,
    número_pasaporte VARCHAR(255),
    nombre_pasajero VARCHAR(255)
);
```

Mientras que la tabla ``vuelos_pasajeros`` se encargará de relacionar los vuelos con los pasajeros y asignarles un asiento.

```sql
CREATE TABLE Vuelos_Pasajeros (
    id_vuelo INT,
    id_pasajero INT,
    n_asiento VARCHAR(255),
    PRIMARY KEY (id_vuelo, id_pasajero),
    FOREIGN KEY (id_vuelo) REFERENCES Vuelos(id_vuelo),
    FOREIGN KEY (id_pasajero) REFERENCES Pasajeros(id_pasajero),
    UNIQUE (id_vuelo, n_asiento)
);
```

Al crear las tablas hemos utilizado dos tipos de claves distintas, las claves primarias y las claves foráneas. Las claves primarias se las asignamos a un registro para hacerlo unico en la tabla. Por ejemplo, en la tabla vuelos la clave primaria es ``id_vuelo``, por lo tanto, dos vuelos no podrán tener el mismo id. Por otro lado, la calve foránea la usamos para relacionar a un registro único de otra tabla que utilice una clave primária. Además, en la tabla ``Vuelos_Pasajeros`` hemos usado alguna clave foránea. Con estas indicamos que, en el primer caso, el registro ``id_vuelo`` de esta tabla debe corresponder al registro ``id_vuelo`` de la tabla ``Vuelos``. Con lo que nos aseguraremos que no se podrán hacer reservas para vuelos que no existan.

Al definir estas restricciones pueden surgin algunos inconvenientes al igual que algunas ventajas. Si quisieramos cancelar un vuelo con muchas reservas puede ser difícil ya que si un vuelo tiene reservas no podemos eliminar el registro de vuelo por culpa de las claves foráneas. Sin embargo, podemos evitar que se elimine un vuelo si este tiene alguna reserva. También, al tener ``n_asiento`` como registro único podemos evitar que dos personas tengan el mismo asiento en un vuelo. Por otra lado, a la hora de asignar un pasajero a un vuelo inexistente, con las claves foráneas evitamos que se hagan reservas para pasajeros y vuelos que no existen en la base de datos. Pero, para ello debemos tener la información actualizada para evitar errores. Por último, el SGBD debería manegar situaciones como las cancelaciones de vuelos, debería cancelar todas las reservas o si el vuelo cambia de capacidad y caben más o menos pasajeros ajustar las reservas.