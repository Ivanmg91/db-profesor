# Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".
use Chinook;
SELECT InvoiceDate, Total
FROM Invoice
WHERE CustomerId = (SELECT CustomerId FROM Customer WHERE Email = "emma_jones@hotmail.com")
ORDER BY Total Desc
LIMIT 5;