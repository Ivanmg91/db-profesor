USE Chinook;

SELECT e.FirstName, e.LastName, 
    (SELECT COUNT(DISTINCT c.CustomerId) 
     FROM Customer c 
     WHERE c.SupportRepId = e.EmployeeId) AS 'Numero de clientes'
FROM Employee e
ORDER BY e.EmployeeId;
