# Obtener los álbumes con una duración total superior a la media.

use Chinook;
SELECT Album.Title, SUM(Track.Milliseconds) AS 'Duración total'
FROM Album
JOIN Track ON Album.AlbumId = Track.AlbumId
GROUP BY Album.AlbumId
HAVING SUM(Track.Milliseconds) > (SELECT AVG(DuracionTotal) FROM (SELECT AlbumId, SUM(Milliseconds) AS 'DuracionTotal' FROM Track GROUP BY AlbumId) AS Subconsulta);