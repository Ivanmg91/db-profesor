#Obtener las canciones con una duración superior a la media.

use Chinook;
SELECT Name
FROM Track
WHERE Milliseconds > (SELECT avg(Milliseconds) FROM Track)
LIMIT 10;