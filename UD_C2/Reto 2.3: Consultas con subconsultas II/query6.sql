# Obtener los álbumes con un número de canciones superiores a la media.

use Chinook;
SELECT Album.Title, COUNT(Track.TrackId) AS 'Número de canciones'
FROM Album
JOIN Track ON Album.AlbumId = Track.AlbumId
GROUP BY Album.AlbumId
HAVING COUNT(Track.TrackId) > (SELECT AVG(NumCanciones) FROM (SELECT AlbumId, COUNT(TrackId) AS 'NumCanciones' FROM Track GROUP BY AlbumId) AS Subconsulta);