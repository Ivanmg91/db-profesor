# Álbumes que tienen más de 15 canciones, junto a su artista.

use Chinook;
SELECT Album.Title, Artist.Name
FROM Album
JOIN Artist ON Album.ArtistId = Artist.ArtistId
WHERE Album.AlbumId IN (SELECT AlbumId FROM Track GROUP BY AlbumId HAVING COUNT(TrackId) > 15);