# Canciones del género con más canciones.

USE Chinook;

SELECT Genre.Name, COUNT(Track.TrackId) AS "Numero de canciones"
FROM Genre
    JOIN Track ON Genre.GenreId = Track.GenreId
GROUP BY
    Genre.GenreId
HAVING
    COUNT(Track.TrackId) = (
        SELECT MAX(CountTracks)
        FROM (
                SELECT Genre.GenreId, COUNT(Track.TrackId) AS CountTracks
                FROM Genre
                    JOIN Track ON Genre.GenreId = Track.GenreId
                GROUP BY
                    Genre.GenreId
            ) AS Subquery
    )
ORDER BY Genre.Name;