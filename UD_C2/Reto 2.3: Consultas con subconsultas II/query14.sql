# Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

USE Chinook;

SELECT ar.Name, 
    (SELECT al.Title 
     FROM Album al 
     WHERE al.ArtistId = ar.ArtistId 
     ORDER BY al.AlbumId DESC 
     LIMIT 1) AS 'Album mas reciente'
FROM Artist ar
ORDER BY ar.ArtistId;