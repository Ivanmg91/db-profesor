# Canciones de la _playlist_ con más canciones.

use Chinook;
SELECT Playlist.Name, COUNT(PlaylistTrack.TrackId) AS "Numero de canciones"
FROM Playlist
JOIN PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
GROUP BY Playlist.PlaylistId
ORDER BY COUNT(PlaylistTrack.TrackId) DESC
LIMIT 1;