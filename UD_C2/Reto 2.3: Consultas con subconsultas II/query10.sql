# Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
use Chinook;

SELECT 
    c.CustomerId, 
    c.FirstName, 
    c.LastName, 
    (SELECT SUM(i.Total) 
     FROM Invoice i 
     WHERE i.CustomerId = c.CustomerId) AS "Total"
FROM 
    Customer c;