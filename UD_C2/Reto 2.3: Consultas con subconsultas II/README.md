# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)

Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Consulta 1
Obtener las canciones con una duración superior a la media.

```sql
SELECT Name
FROM Track
WHERE Milliseconds > (SELECT avg(Milliseconds) FROM Track)
LIMIT 10;
```

### Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".

```sql
SELECT InvoiceDate, Total
FROM Invoice
WHERE CustomerId = (SELECT CustomerId FROM Customer WHERE Email = "emma_jones@hotmail.com")
ORDER BY Total Desc
LIMIT 5;
```


## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.

```sql
SELECT Name
FROM Playlist
WHERE PlaylistId IN (SELECT PlaylistId FROM PlaylistTrack WHERE TrackId IN (SELECT TrackId FROM Track WHERE GenreId = (SELECT GenreId FROM Genre WHERE Name = "Reggae")));
```

### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20€.

```sql
SELECT FirstName, LastName
FROM Customer
WHERE CustomerId IN (SELECT CustomerId FROM Invoice WHERE Total > 20);
```

### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.

```sql
SELECT Album.Title, Artist.Name
FROM Album
JOIN Artist ON Album.ArtistId = Artist.ArtistId
WHERE Album.AlbumId IN (SELECT AlbumId FROM Track GROUP BY AlbumId HAVING COUNT(TrackId) > 15);
```

### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.

```sql
SELECT Album.Title, COUNT(Track.TrackId) AS 'Número de canciones'
FROM Album
JOIN Track ON Album.AlbumId = Track.AlbumId
GROUP BY Album.AlbumId
HAVING COUNT(Track.TrackId) > (SELECT AVG(NumCanciones) FROM (SELECT AlbumId, COUNT(TrackId) AS 'NumCanciones' FROM Track GROUP BY AlbumId) AS Subconsulta);
```

### Consulta 7
Obtener los álbumes con una duración total superior a la media.

```sql
SELECT Album.Title, SUM(Track.Milliseconds) AS 'Duración total'
FROM Album
JOIN Track ON Album.AlbumId = Track.AlbumId
GROUP BY Album.AlbumId
HAVING SUM(Track.Milliseconds) > (SELECT AVG(DuracionTotal) FROM (SELECT AlbumId, SUM(Milliseconds) AS 'DuracionTotal' FROM Track GROUP BY AlbumId) AS Subconsulta);
```

### Consulta 8
Canciones del género con más canciones.

```sql
SELECT Genre.Name, COUNT(Track.TrackId) AS "Numero de canciones"
FROM Genre
    JOIN Track ON Genre.GenreId = Track.GenreId
GROUP BY
    Genre.GenreId
HAVING
    COUNT(Track.TrackId) = (
        SELECT MAX(CountTracks)
        FROM (
                SELECT Genre.GenreId, COUNT(Track.TrackId) AS CountTracks
                FROM Genre
                    JOIN Track ON Genre.GenreId = Track.GenreId
                GROUP BY
                    Genre.GenreId
            ) AS Subquery
    )
ORDER BY Genre.Name;
```

### Consulta 9
Canciones de la _playlist_ con más canciones.

```sql
SELECT Playlist.Name, COUNT(PlaylistTrack.TrackId) AS "Numero de canciones"
FROM Playlist
JOIN PlaylistTrack ON Playlist.PlaylistId = PlaylistTrack.PlaylistId
GROUP BY Playlist.PlaylistId
ORDER BY COUNT(PlaylistTrack.TrackId) DESC
LIMIT 1;
```


## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]

### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

```sql
SELECT 
    c.CustomerId, 
    c.FirstName, 
    c.LastName, 
    (SELECT SUM(i.Total) 
     FROM Invoice i 
     WHERE i.CustomerId = c.CustomerId) AS "Total"
FROM 
    Customer c;
```

### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

```sql
SELECT e.FirstName, e.LastName, 
    (SELECT COUNT(DISTINCT c.CustomerId) 
     FROM Customer c 
     WHERE c.SupportRepId = e.EmployeeId) AS 'Numero de clientes'
FROM Employee e
ORDER BY e.EmployeeId;
```

### Consulta 12
Ventas totales de cada empleado.

```sql
SELECT e.FirstName, e.LastName, 
    (SELECT SUM(i.Total) 
     FROM Invoice i 
     WHERE i.CustomerId IN (SELECT c.CustomerId FROM Customer c WHERE c.SupportRepId = e.EmployeeId)) AS 'Ventas totales'
FROM Employee e
ORDER BY e.EmployeeId;
```

### Consulta 13
Álbumes junto al número de canciones en cada uno.

```sql
SELECT a.Title, 
    (SELECT COUNT(t.TrackId) 
     FROM Track t 
     WHERE t.AlbumId = a.AlbumId) AS 'Numero de canciones'
FROM Album a
ORDER BY a.AlbumId;
```

### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

```sql
SELECT ar.Name, 
    (SELECT al.Title 
     FROM Album al 
     WHERE al.ArtistId = ar.ArtistId 
     ORDER BY al.AlbumId DESC 
     LIMIT 1) AS 'Album mas reciente'
FROM Artist ar
ORDER BY ar.ArtistId;
```

## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/