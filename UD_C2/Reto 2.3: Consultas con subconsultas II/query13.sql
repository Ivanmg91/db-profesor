# Álbumes junto al número de canciones en cada uno.

USE Chinook;

SELECT a.Title, 
    (SELECT COUNT(t.TrackId) 
     FROM Track t 
     WHERE t.AlbumId = a.AlbumId) AS 'Numero de canciones'
FROM Album a
ORDER BY a.AlbumId;
