# Ventas totales de cada empleado.

USE Chinook;

SELECT e.FirstName, e.LastName, 
    (SELECT SUM(i.Total) 
     FROM Invoice i 
     WHERE i.CustomerId IN (SELECT c.CustomerId FROM Customer c WHERE c.SupportRepId = e.EmployeeId)) AS 'Ventas totales'
FROM Employee e
ORDER BY e.EmployeeId;
