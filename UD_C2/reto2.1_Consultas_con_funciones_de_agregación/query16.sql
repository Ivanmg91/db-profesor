# Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).

use Chinook;
SELECT G.Name AS "Género", COUNT(T.TrackId) AS "Número de canciones"
FROM Genre G
JOIN Track T ON G.GenreId = T.GenreId
GROUP BY G.Name;