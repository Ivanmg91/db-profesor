use Chinook;
SELECT 
	I.InvoiceDate AS "Fecha de la factura",
    concat(FirstName, " ", LastName) AS "Nombre completo",
    I.BillingAddress AS "Dirección de facturación",
    C.PostalCode AS "Código postal",
    I.Total AS "Importe"
FROM Customer C
JOIN Invoice I ON C.CustomerId = I.InvoiceId;