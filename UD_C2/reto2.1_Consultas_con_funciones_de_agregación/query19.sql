# Lista los 6 álbumes que acumulan más compras.

use Chinook;
SELECT A.Title AS "Álbum", COUNT(I.InvoiceId) AS "Total de compras"
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
JOIN InvoiceLine IL ON T.TrackId = IL.TrackId
JOIN Invoice I ON IL.InvoiceId = I.InvoiceId
GROUP BY A.Title
ORDER BY COUNT(I.InvoiceId) DESC
LIMIT 6;