use Chinook;
SELECT Artist.Name, Album.Title
FROM Album
JOIN Artist ON Artist.ArtistId = Album.AlbumId;