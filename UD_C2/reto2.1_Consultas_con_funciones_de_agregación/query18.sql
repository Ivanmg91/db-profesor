# Encuentra los géneros musicales más populares (los más comprados).

use Chinook;
SELECT G.Name AS "Género", COUNT(I.InvoiceId) AS "Total compras"
FROM Genre G
JOIN Track T ON G.GenreId = T.GenreId
JOIN InvoiceLine IL ON T.TrackId = IL.TrackId
JOIN Invoice I ON IL.InvoiceId = I.InvoiceId
GROUP BY G.Name
ORDER BY COUNT(I.InvoiceId) DESC;