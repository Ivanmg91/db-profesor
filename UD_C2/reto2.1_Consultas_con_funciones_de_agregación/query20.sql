# Muestra los países en los que tenemos al menos 5 clientes.

use Chinook;
SELECT C.Country AS "País", COUNT(C.CustomerId) AS "Número de clientes"
FROM Customer C
GROUP BY C.Country
HAVING COUNT(C.CustomerId) >= 5
ORDER BY COUNT(C.CustomerId) DESC;