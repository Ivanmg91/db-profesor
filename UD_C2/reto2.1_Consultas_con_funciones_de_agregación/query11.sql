# Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.

use Chinook;
SELECT * 
FROM Customer C
JOIN Invoice I ON C.CustomerId = I.InvoiceId
WHERE I.Total > 10;