# Muestra el número total de artistas

use Chinook;
SELECT COUNT(ArtistId) AS "Total de artistas"
FROM Artist;

# En este caso específico podría servir sacar el id más alto de la tabla Artist para saber cuántos artistas hay en total.
SELECT MAX(ArtistId) AS "ArtistId más alto"
FROM Artist;