# Reto 3: Definición de datos en MySQL

Iván Montiano González.

En este reto documentaremos el proceso para realizar las querys porpuestas en el reto 2.1. En la que utilizamos la base de datos Chinook, que se encuentra en el repositorio: https://github.com/lerocha/chinook-database/.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Ivanmg91/db-profesor/-/tree/main/UD_C2/reto2.1_Consultas_con_funciones_de_agregaci%C3%B3n?ref_type=heads

## Query 1
Encuentra todos los clientes de Francia. Para ello hacemos un `SELECT` indicando lo que queremos, en este caso hemos seleccionado todo, con `FROM` indicando la tabla y usando `WHERE` para poner la condición.

```sql
use Chinook;
SELECT * FROM Chinook.Customer
WHERE Country LIKE "France";
```

## Query 2
Muestra las facturas del primer trimestre de este año. Hacemos un `SELECT` indicando lo que queremos, en este caso hemos seleccionado todo, con `FROM` indicando la tabla y usando `WHERE` y `AND` para poner la condición, usando la funcion `month` para indicar el mes de una fecha y `year` para el año.

```sql
use Chinook;
SELECT * FROM Chinook.Invoice
WHERE month(InvoiceDate) <= 3 
AND year(InvoiceDate) = year(now());
```

## Query 3
Muestra todas las canciones compuestas por AC/DC. Hacemos `SELECT` y `FROM` como siempre y `WHERE`, y dentro de este ponemos la condición.

```sql
use Chinook;
SELECT * FROM Chinook.Track
WHERE Composer = "AC/DC";
```

## Query 4
Muestra las 10 canciones que más tamaño ocupan. Usamos `SELECT` y `WHERE` para mostrar lo que queremos de la tabla que le indiquemos. Luego con `ORDER BY` lo ordenamos por la tabla "Bytes" y `DESC` para que sea decreciente. Con `LIMIT` mostramos solo los 10 primeros.

```sql
use Chinook;
SELECT * FROM Chinook.Track
ORDER BY Bytes DESC
LIMIT 10;
```

## Query 5
Muestra el nombre de aquellos países en los que tenemos clientes. Usamos `SELECT DISTINCT` para mostrar todos lo que haya dentro del campo seleccionado sin repetir y `FROM` para indicar la tabla.

```sql
use Chinook;
SELECT DISTINCT Country
FROM Chinook.Customer;
```

## Query 6
Muestra todos los géneros musicales. Usando `SELECT` y `FROM` cogemos la columna de "Name" de la tabla "Genre".

```sql
use Chinook;
SELECT Name FROM Chinook.Genre;
```

## Query 7
Muestra las 10 canciones que más tamaño ocupan. Muestra todos los artistas junto a sus álbumes. Usando `SELECT` y `FROM` cogemos la columna y la tabla. Con `JOIN` enlazamos las tablas "Artist" y "Album" mediante su columna en comúm "AlbumId". Así imprimimos el nombre de una tabla y el titulo de otra cuando la columna en común coincida.

```sql
use Chinook;
SELECT Artist.Name, Album.Title
FROM Album
JOIN Artist ON Artist.ArtistId = Album.AlbumId;
```

## Query 8
Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de sus supervisores, si los tienen. Usamos `SELECT` con `AS` para mostrar columnas con alias. Luego, unimos las tablas clientes y pedidos utilizando la condición "clientes.id" = "pedidos.cliente_id". El `LEFT JOIN` asegura que las filas de la tabla clientes se incluyan en el resultado, incluso si no hay coincidencias en la tabla pedidos.

```sql
use Chinook;
SELECT E.FirstName AS "Empleado", M.FirstName AS "Supervisor"
FROM Employee E
LEFT JOIN Employee M ON E.ReportsTo = M.EmployeeId
ORDER BY E.BirthDate DESC
LIMIT 15;
```

## Query 9
Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las columnas: fecha de la factura, nombre completo del cliente, dirección de facturación, código postal, país, importe (en este orden). Para resolver esta query, dentro del `SELECT` usamos la funcion `concat()` para unir las columnas "FirstName" y "LastName" y que las muestre como nombre completo. Al final, con `JOIN` unimos las tablas "Invoices" y "Customers" mediante "CustomerId" y "InvoiceId". 

```sql
use Chinook;
SELECT 
	I.InvoiceDate AS "Fecha de la factura",
    concat(FirstName, " ", LastName) AS "Nombre completo",
    I.BillingAddress AS "Dirección de facturación",
    C.PostalCode AS "Código postal",
    I.Total AS "Importe"
FROM Customer C
JOIN Invoice I ON C.CustomerId = I.InvoiceId;
```

## Query 10
Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas sus canciones, ordenadas por álbum y por duración. En esta query usamos varios `JOIN` para conectar varias tablas entre sí, en el primero unimos las tablas "Playlists" con "PlaylistTrack", en el segundo "PlaylistTrack" con "Track" y por último "Track" con "Album". Así podemos obtener las listas de reproducción que comienzan "C", junto con todas sus canciones, ordenadas por álbum y duración.

```sql
use Chinook;
SELECT P.Name AS "Lista de reproducción", T.Name AS "Nombre de la canción", A.Title AS "Título del album"
FROM
    Playlist P
    JOIN PlaylistTrack PT ON P.PlaylistId = PT.PlaylistId
    JOIN Track T ON PT.TrackId = T.TrackId
    JOIN Album A ON T.AlbumId = A.AlbumId
WHERE
    P.Name LIKE "C%"
ORDER BY A.Title, T.Milliseconds;
```


## Query 11
Muestra las 10 canciones que más tamaño ocupan. En esta consulta hacemos un `JOIN` para enlazar "CustomerId" de la tabla "Customer" y "InvoiceId" de la tabla "Invoice". Para acabar con `WHERE` inidcamos que cuando el la columna "Total" de "Invoice" sea mayor que 10.

```sql
use Chinook;
SELECT * 
FROM Customer C
JOIN Invoice I ON C.CustomerId = I.InvoiceId
WHERE I.Total > 10;
```

## Query 12
Muestra el importe medio, mínimo y máximo de cada factura. Usamos las funciones "AVG", "MIN" y "MAX" dentro del `SELECT` para sacar la media, el minimo y el máximo importe de las facturas.

```sql
use Chinook;
SELECT 
    AVG(Total) AS "Importe medio",
    MIN(Total) AS "Importe mínimo",
    MAX(Total) AS "Importe máximo"
FROM Invoice
GROUP BY InvoiceId;
```
## Query 13
Muestra el número total de artistas. Para hacerlo usamos la función `COUNT` indicando la columna "ArtistId" para que muestre cuantos artistas hay.

```sql
use Chinook;
SELECT COUNT(ArtistId) AS "Total de artistas"
FROM Artist;
```

## Query 14
Muestra el número de canciones del álbum “Out Of Time”. Usamos `COUNT` para contar el número de canciones. Luego con `JOIN` enlazamos las columnas de ambas tablas "AlbumId" y en el `WHERE` ponemos la condición de que cuando el titulo sea el que queremos. 


```sql
use Chinook;
SELECT COUNT(TrackId) AS "Número de canciones"
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
WHERE A.Title = "Out Of Time";
```

## Query 15
Muestra el número de países donde tenemos clientes. Hacemos un `COUNT` y dentro usamos `DISTINCT` para que no cuente los valores repetidos.

```sql
use Chinook;
SELECT count(DISTINCT Country) AS "Número de países"
FROM Customer;
```

## Query 16
Muestra el número de canciones de cada género (deberá mostrarse el nombre del género). Hacemos un `SELECT` para sacar lo que nos piden de la tabla "Genre" y  con `JOIN` unimos las columnas "GenreId" de las tablas "Genre" y "Track".

```sql
use Chinook;
SELECT G.Name AS "Género", COUNT(T.TrackId) AS "Número de canciones"
FROM Genre G
JOIN Track T ON G.GenreId = T.GenreId
GROUP BY G.Name;
```

## Query 17
Muestra los álbumes ordenados por el número de canciones que tiene cada uno. Dentro del `SELECT` indicamos varios alias para hacerlo más arreglado, además de un `COUNT` para contar el número de canciones. Con `JOIN` ligamos las columnas "AlbumId" de las tablas "Track" y "Album". Agrupamos con `GROUP BY` y ordenamos de manera descendente con `ORDER BY`.

```sql
use Chinook;
SELECT A.Title AS "Álbum", COUNT(T.TrackId) AS "Número de canciones"
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.Title
ORDER BY COUNT(T.TrackId) DESC;
```

## Query 18
Encuentra los géneros musicales más populares (los más comprados). Para ello necesitaremos hacer tres `JOIN` para conseguir enlazar el numero de ventas con el género para sacar el tipo de género más comprado. Uniremos las tablas "Genre" y "Track" mediante "GenreId", las tablas "Track" y "InvoiceLine" mediante "TrackId" y, por último, las tablas "InvoiceLine" y "Invoice" con "InvoiceId".

```sql
use Chinook;
SELECT G.Name AS "Género", COUNT(I.InvoiceId) AS "Total compras"
FROM Genre G
JOIN Track T ON G.GenreId = T.GenreId
JOIN InvoiceLine IL ON T.TrackId = IL.TrackId
JOIN Invoice I ON IL.InvoiceId = I.InvoiceId
GROUP BY G.Name
ORDER BY COUNT(I.InvoiceId) DESC;
```

## Query 19
Lista los 6 álbumes que acumulan más compras. Para hacerlo debemos seguir a la anterior query como ejemplo para sacar los 6 álbumes más comprados. Usaremos `JOIN` para unir las tablas y `LIMIT` para limitar los resultados.

```sql
use Chinook;
SELECT A.Title AS "Álbum", COUNT(I.InvoiceId) AS "Total de compras"
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
JOIN InvoiceLine IL ON T.TrackId = IL.TrackId
JOIN Invoice I ON IL.InvoiceId = I.InvoiceId
GROUP BY A.Title
ORDER BY COUNT(I.InvoiceId) DESC
LIMIT 6;
```

## Query 20
Muestra los países en los que tenemos al menos 5 clientes. Para sacar esta query tendremos que usar `HAVING COUNT` para filtrar los resultados de la consulta y mostrar solo los países que tienen al menos 5 clientes y los ordenaremos con `ORDER BY COUNT() DESC` para que ordene de manera descendiente segun el id del cliente.

```sql
SELECT C.Country AS "País", COUNT(C.CustomerId) AS "Número de clientes"
FROM Customer C
GROUP BY C.Country
HAVING COUNT(C.CustomerId) >= 5
ORDER BY COUNT(C.CustomerId) DESC;
```