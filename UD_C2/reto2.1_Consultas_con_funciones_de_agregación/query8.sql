USE Chinook;
SELECT E.FirstName AS "Empleado", M.FirstName AS "Supervisor"
FROM Employee E
LEFT JOIN Employee M ON E.ReportsTo = M.EmployeeId
ORDER BY E.BirthDate DESC
LIMIT 15;