# Muestra los álbumes ordenados por el número de canciones que tiene cada uno.

use Chinook;
SELECT A.Title AS "Álbum", COUNT(T.TrackId) AS "Número de canciones"
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
GROUP BY A.Title
ORDER BY COUNT(T.TrackId) DESC;