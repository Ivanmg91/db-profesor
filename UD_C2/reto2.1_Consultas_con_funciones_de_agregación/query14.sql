# Muestra el número de canciones del álbum “Out Of Time”.

use Chinook;
SELECT COUNT(TrackId) AS "Número de canciones"
FROM Album A
JOIN Track T ON A.AlbumId = T.AlbumId
WHERE A.Title = "Out Of Time";