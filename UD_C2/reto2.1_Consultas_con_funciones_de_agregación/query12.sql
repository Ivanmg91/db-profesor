# Muestra el importe medio, mínimo y máximo de cada factura.

use Chinook;
SELECT 
    AVG(Total) AS "Importe medio",
    MIN(Total) AS "Importe mínimo",
    MAX(Total) AS "Importe máximo"
FROM Invoice
GROUP BY InvoiceId;