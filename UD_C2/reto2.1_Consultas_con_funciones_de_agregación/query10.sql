use Chinook;

SELECT P.Name AS "Lista de reproducción", T.Name AS "Nombre de la canción", A.Title AS "Título del album"
FROM
    Playlist P
    JOIN PlaylistTrack PT ON P.PlaylistId = PT.PlaylistId
    JOIN Track T ON PT.TrackId = T.TrackId
    JOIN Album A ON T.AlbumId = A.AlbumId
WHERE
    P.Name LIKE "C%"
ORDER BY A.Title, T.Milliseconds;