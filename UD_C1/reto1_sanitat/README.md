# Reto 1: Consultas básicas

Iván Montiano González.

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Ivanmg91/db-profesor/-/tree/main/UD_C1/reto1_sanitat

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Utilizamos `USE` para seleccionar especificar la base de datos que queremos usar y para una mayor claridad añadimos alias en referencia a la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;
SELECT 
	H.HOSPITAL_COD código,
	H.NOM nombre,
	H.TELEFON telefono
FROM HOSPITAL H;
```


## Query 2
Para seleccionar el contenido de las columnas `número`, `nombre` y `teléfono` de todos los hospitales que tengan una letra A en la segunda posición del nombre seguiremos el modelo de query anterior y añadiremos un `WHERE` para referiros a una columna y `LIKE` "_a%". Usamos "_a%" para indicar que en el segundo caracter debe haber una "a" y con el "%" indicamos que nos da igual lo que siga.

```sql
USE sanitat;
SELECT 
	H.HOSPITAL_COD código,
	H.NOM nombre,
	H.TELEFON telefono
FROM HOSPITAL H
WHERE nom LIKE "_a%";
```

Otra forma de hacerlo sería utilizando el `substring`, donde el 2 es la posición del string y el 1 lo que hace es coger 1 carácter a partir de la segunda posición(2): 
```sql
USE sanitat;
SELECT HOSPITAL_COD, NOM, TELEFON
FROM HOSPITAL 
WHERE substr(NOM, 2, 1) = 'a';
```

## Query 3
Para seleccionar el contenido de las columnas `código hospital`, `código sala`, `número empleado` y `apellido` de todos los trabajadores usaremos un `SELECT` indicando las columnas que queremos y `FROM` para referirnos a la tabla que necesitamos.

```sql
USE sanitat;
SELECT hospital_cod, sala_cod, empleat_no, cognom
FROM PLANTILLA;
```

## Query 4
Para selecconar el contenido de las columnas `código hospital`, `código sala`, `número empleado` y
`apellido` de los trabajadores que no pertenezcan al turno de noche utilizamos `SELECT` para seleccionar las columnas y `FROM` PARA LA TABLA. Finalmente usamos `WHERE` como condicional para cuando la columna `torn` no sea "n".

```sql
USE sanitat;
SELECT TORN, HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA
WHERE TORN NOT LIKE "N";
```

Podríamos cambiar `NOT LIKE` por `<>` o bien por `!=` y tambíen nos funcionaría.

## Query 5
Para mostrar a los enfermos nacidos en el año 1960 usamos `SELECT` para seleccionar todas las columnas, `FROM` haciendo referencia a la tabla `malalt` y dentro de `WHERE` utilizamos la funcion `year` para sacar el año, ya que esta función coge el año de una fecha y como `data_naix` es formato fecha podemos utilizar la función.

```sql
USE sanitat;
SELECT *
FROM MALALT
WHERE year(data_naix) = 1960;
```

En lugar de la función `YEAR` podriamos usar un between:

```sql
USE sanitat;
SELECT *
FROM MALALT
WHERE DATA_NAIX BETWEEN '1960-01-01' AND '1960-12-31';
```

## Query 6
Para mostrar a los enfermos nacidos a partir de 1960 podemos usar la query anterior añadiendo el operador `>` para decirle que queremos los años mayores al que le indicamos.

```sql
SELECT INSCIPCIO, COGNOM, ADRECA, DATA_NAIX
FROM MALALT
WHERE year(data_naix) > 1960;
```

Otra opción de hacerlo sin usar la función `year` y el operador `>` sería con un between y la función `current_date()` que usa la fecha en la que estamos.

```sql
SELECT INSCIPCIO, COGNOM, ADRECA, DATA_NAIX
FROM MALALT
WHERE DATA_NAIX BETWEEN '1960-01-01' AND current_date();
```