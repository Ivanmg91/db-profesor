# Reto 1: Consultas básicas

Iván Montiano González.

En este reto trabajamos con la base de datos `Empresa` y `Sanitat`, que nos vienen dadas en el fichero `empresa.sql` y `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados. Además, hay algunas consulas extras realizadas en clase.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/Ivanmg91/db-profesor/-/tree/main/UD_C1/reto2_Consultas_b%C3%A1sicas_2

## EMPRESA:

## Query 1
Para mostrar el código y descripción  de los productos que comercializa la empresa usamos un `SELECT` con las columnas que queremos mostrar y `FROM` para indicar la tabla.

```sql
USE empresa;
SELECT PROD_NUM, DESCRIPCIO
FROM PRODUCTE;
```

## Query 2
Para mostrar los productos que contienen la palabra tennis en la descripción usamos un `SELECT` con las columnas que queremos mostrar, `FROM` para indicar la tabla y `WHERE` para decirle cuando el contenido de la columna ("DESCRIPCIO") sea como, usando `LIKE`, "%TENNIS%". Usando los "%" para indicar que hay algo delante y detras de tennis. 

```sql
USE empresa;
SELECT PROD_NUM, DESCRIPCIO
FROM PRODUCTE
WHERE DESCRIPCIO LIKE "%TENNIS%";
```

## Query 3
Para mostrar el código, nombre, área y telefono de los clientes de la empresa usamos `SELECT` para indicar las columnas y `FROM` para indicar la tabla.

```sql
USE empresa;
SELECT CLIENT_COD, NOM, AREA, TELEFON
FROM CLIENT;
```
## Query 4
Para mostrar los clientes que no pertenecen al área telefónica 636 indicamos las columnas con `SELECT` y la tabla con `FROM` Y `WHERE` para decirle cuando el contenido de la columna no sea como, usando `NOT LIKE`, "636". 

```sql
USE empresa;
SELECT CLIENT_COD, NOM, CIUTAT, AREA
FROM CLIENT
WHERE AREA NOT LIKE 636;
```

## Query 5
Para mostrar las órdenes de compra de la tabla de pedidos usamos `SELECT` para indicar las columnas y `FROM` para indicar la tabla.

```sql
USE empresa;
SELECT CLIENT_COD, COM_DATA, DATA_TRAMESA
FROM COMANDA
```

## VIDEOCLUB:

## Query 6
Para mostrar una lista de nombres y teléfonos de los clientes usamos `SELECT` para indicar las columnas y `FROM` para indicar la tabla.

```sql
USE videoclub;
SELECT Nom, Telefon
FROM CLIENT;
```

## Query 7
Para mostrar una lista de fechas e importes de las facturas usamos `SELECT` para indicar las columnas y `FROM` para indicar la tabla.

```sql
USE videoclub;
SELECT Data, Import
FROM FACTURA;
```

## Query 8
Para mostrar una lista de productos facturados en la factura número 3 usamos `SELECT` para indicar las columnas y `FROM` para indicar la tabla y `WHERE` para decirle cuando el contenido de la columna sea "3".

```sql
USE videoclub;
SELECT Descripcio
FROM DETALLFACTURA
WHERE CodiFactura LIKE 3;
```

## Query 9
Para mostrar una lista ordenada de forma decreciente de las facturas por importe usamos `ORDER BY` y la columna y `DESC` para indicarle que sea descendente.

```sql
SELECT *
FROM FACTURA
ORDER BY Import DESC;
```

## Query 10
Para mostrar una lista de los actores que su nombre comience por "X" usamos `SELECT` para indicar las columnas y `FROM` para indicar la tabla y `WHERE` para decirle cuando el contenido de la columna sea "X%". Usamos "%" para indicarle que contiene más carácteres detrás.

```sql
USE videoclub;
SELECT *
FROM ACTOR
WHERE Nom LIKE "X%";
```

## Query 11
Añadimos actores usando `INSERT INTO` donde indicamos la tabla y entre paréntesis las columnas que queremos añadir. Lueogo usamos `VALUES` para añadir el contenido a las columnas. Cambiando los valores que se encuentran dentro de `VALUES` añadiriamos más actores.

```sql
INSERT INTO ACTOR (Nom, CodiActor)
VALUES ("Xordi", 6);
```

## Query 12
Si queremos eliminar algun actor, como en este caso a Xordi, usamos `DELETE FROM` inidcando la tabla y `WHERE` indicando la tabla y la condicion. En este caso concreto como la tabla está en modo seguro no nos permite eliminar usando el nombre "Xordi" y utilizamos la columna "CodiActor" para eliminarlo.

```sql
USE videoclub;
DELETE FROM ACTOR WHERE CodiActor = 6;
```

## Query 13
En este caso queremos elimiar a más de un actor a la vez, para ello con el `WHERE` indicamos la columna y con `IN` lo que hacemos es pasarle varios valores a la vez.

```sql
USE videoclub;
DELETE FROM ACTOR 
WHERE CodiActor IN (4, 5);
```

## Query 14
Ahora lo que queremos es insertar varios actores a la vez, entoces usamos `VALUES` y añadimos los valores de cada columna, ordenados respectivamente.

```sql
INSERT INTO ACTOR
VALUES
(4, "Ximo"),
(5, "Xavi");
```

## Query 15
Si queremos cambiar un registro sin eliminarlo usaremos `SET` y la columna que queremos modificar, para inidcarle el valor modificado y, en este caso, `WHERE` para todos aquellos actores que en la columna "CodiActor" tengan el valor 6.

```sql
UPDATE ACTOR
SET Nom = "Jordi"
WHERE CodiActor = 6;
```
## Query 16
Si nos gustaría saber en que años hemos dado de alta a trabajadores, usamos `SELECT DISTINCT`. Con `DISTINCT` hacemos que nos devuelva valores sin que se repitan y con la función `year()` especificamos de una fecha que lo que queremos es el año. 

```sql
USE empresa;
SELECT DISTINCT year(DATA_ALTA)
FROM EMP;
```

## Query 17
En este caso queremos ver las distintas categorías de empleados que tenemos actualmente. Para ello, usamos  `SELECT DISTINCT` y la columna "OFICI" para mostrar los valores de esa columna sin repetirse. 

```sql
USE empresa;
SELECT DISTINCT OFICI
FROM EMP;
```

## Query 18
Para contar cuantas categorías diferentes hay, usamos la función `count()` y le pasamos las distintas categorías usando `DISTINCT` y la columna que queremos. 

```sql
USE empresa;
SELECT count(DISTINCT OFICI)
FROM EMP;
```

## Query 19 20
Para mostrar a los empleados que van a comisión usamos `WHERE` y la columna que queremos y al final usamos `IS NOT NULL` para que muestre los empleados cuya comisión no sea null.

```sql
USE empresa;
SELECT *
FROM EMP
WHERE COMISSIO IS NOT NULL;
```

Si quisieramos mostrar los que no van a comisión usariamos `IS NULL`.
```sql
USE empresa;
SELECT *
FROM EMP
WHERE COMISSIO IS NULL;
```

## Query 21
Para mostrar a los empleados que van a comisión usamos `WHERE` y la columna que queremos y al final usamos `IS NOT NULL`. Luego, ordenamos con `ORDER BY` de forma descendente con `DESC` y usamos `LIMIT` para limitar los resultados y que solo nos muestre los 2 primeros.

```sql
USE empresa;
SELECT *
FROM EMP
WHERE COMISSIO IS NOT NULL
ORDER BY COMISSIO DESC
LIMIT 2;
```